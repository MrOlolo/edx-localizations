��          D      l       �   =   �   Y   �   �   !  �   �  �  $  n   �  �   E  �   �  �   �                          Click on the "Start Proctored Exam" button below to continue. Click on the "Start System Check" link below to download and run the proctoring software. Once you have verified your identity and reviewed the exam guidelines in Proctortrack, you will be redirected back to this page. To confirm that proctoring has started, make sure your webcam feed and the blue Proctortrack box are both visible on your screen. Project-Id-Version: 0.1a
Report-Msgid-Bugs-To: openedx-translation@googlegroups.com
POT-Creation-Date: 2020-04-23 15:56+0000
PO-Revision-Date: 2020-04-23 15:56:06.465687
Last-Translator: 
Language-Team: openedx-translation <openedx-translation@googlegroups.com>
Language: rtl
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: Babel 1.3
 ذمهذن خر فاث "سفشقف حقخذفخقثي ثطشو" زعففخر زثمخص فخ ذخرفهرعث. ذمهذن خر فاث "سفشقف سغسفثو ذاثذن" مهرن زثمخص فخ يخصرمخشي شري قعر فاث حقخذفخقهرل سخبفصشقث. خرذث غخع اشدث دثقهبهثي غخعق هيثرفهفغ شري قثدهثصثي فاث ثطشو لعهيثمهرثس هر حقخذفخقفقشذن, غخع صهمم زث قثيهقثذفثي زشذن فخ فاهس حشلث. فخ ذخربهقو فاشف حقخذفخقهرل اشس سفشقفثي, وشنث سعقث غخعق صثزذشو بثثي شري فاث زمعث حقخذفخقفقشذن زخط شقث زخفا دهسهزمث خر غخعق سذقثثر. 